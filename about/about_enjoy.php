
    <div class="video_area video_bg overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="video_wrap text-center">
                        <h3>Độc quyền chỉ có tại GrearVN</h3>
                        <div class="video_icon">
                            <a class="popup-video video_play_button" href="https://www.youtube.com/watch?v=1aqI7EnfbVM" >
                            <i class="fa fa-play"></i>
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="travel_variation_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="single_travel text-center">
                        <div class="icon">
                            <img src="img/svg_icon/1.svg" alt="">
                        </div>
                        <h3>Trải nghiệm thoải mái</h3>
                        <p>Mang đến cho bạn cảm nhận, dịch vụ chăm chóc tuyệt vời nhất</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="single_travel text-center">
                        <div class="icon">
                            <img src="img/svg_icon/3.svg" alt="">
                        </div>
                        <h3>Thị trường toàn năng</h3>
                        <p>Liên kết sản phẩm với nước ngoài bao rộng quanh Châu Á</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- testimonial_area  -->
    <div class="testimonial_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="testmonial_active owl-carousel">
                        <div class="single_carousel">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <div class="single_testmonial text-center">
                                        <div class="author_thumb">
                                            <img src="img/testmonial/nhanvien1.png" alt="">
                                        </div>
                                        <p>"Chúng tôi có sức mạnh mang đến nụ cười thực sự của bạn."</p>
                                        <div class="testmonial_author">
                                            <h3>Bùi Huy Hoàng</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_carousel">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <div class="single_testmonial text-center">
                                        <div class="author_thumb">
                                            <img src="img/testmonial/nhanvien2.png" alt="">
                                        </div>
                                        <p>"Chúng tôi cảm thấy niềm vui khi được phục vụ bạn tốt nhất"</p>
                                        <div class="testmonial_author">
                                            <h3>Nguyễn Phi Khanh</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_carousel">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <div class="single_testmonial text-center">
                                        <div class="author_thumb">
                                            <img src="img/testmonial/nhanvien3.png" alt="">
                                        </div>
                                        <p>"Tất cả mối quan tâm của chúng tôi là khách hàng"</p>
                                        <div class="testmonial_author">
                                            <h3>Từ Vĩ Khang</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /testimonial_area  -->

