<!--================Blog Area =================-->
<section class="blog_area single-post-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 posts-list">
                    <div class="single-post">
                        <div class="feature-img">
                            <img class="img-fluid" src="img/blog/picture1.png" alt="">
                        </div>
                        <div class="blog_details">
                            <h2>Thị trường laptop Việt Nam tăng trưởng tốt</h2>
                            <ul class="blog-info-link mt-3 mb-4">
                                <li><a href="#"><i class="fa fa-user"></i> HCM, Huy Hoàng</a></li>
                                <li><a href="#"><i class="fa fa-comments"></i> 03 Comments</a></li>
                            </ul>
                            <p class="excert">
                            Theo hãng nghiên cứu thị trường Gfk, sức mua laptop tại thị trường Việt Nam trong năm 2019 đang có xu hướng gia tăng, và con số này vẫn cao hơn so với năm 2018.
                            </p>
                            <p>
                            Theo Gfk, tổng quan trong 9 tháng đầu năm 2019, thị trường máy tính xách tay tăng 7% về số lượng và 15% về giá trị doanh thu so với cùng kỳ năm 2018.
                            </p>
                            <div class="quote-wrapper">
                                <div class="quotes">
                                Hiện tại, các thương hiệu laptop được người tiêu dùng Việt Nam ưa chuộng gồm: Apple, HP, Lenovo, Acer, Asus, Dell, MSI, Masstel với nhiều phân khúc và giá thành khác nhau. Có những chiếc laptop có giá chỉ vài ba triệu đồng nhưng cũng có máy giá trị hơn 50 triệu đồng.
                                </div>
                            </div>
                            <p>
                            Đối với những mẫu máy giá dưới 10 triệu đồng, chủ yếu máy được thiết kế dành cho dân văn phòng, và những sản phẩm này đều nhỏ gọn, cấu hình không quá cao, phù hợp cho việc soạn thảo văn bản.
                            </p>
                            <p>
                            Trong khi đó, đối với số liệu của hãng nghiên cứu thị trường Gartner, thời điểm vàng tăng trưởng của thị trường laptop rơi vào ba tháng 8, 9 và 10 hằng năm với mức tăng trưởng có thể lên đến 20%. Vì thế, nhận định thị trường laptop thời điểm này bão hòa là một nhận định vội vàng. Bởi lẽ, sau một thời gian chậm tăng trưởng so với hai dòng công nghệ tiện dụng smartphone và tablet, thì laptop đang có sự quay trở lại mạnh mẽ.
                            </p>
                        </div>
                    </div>
                    <div class="navigation-top">
                        <div class="d-sm-flex justify-content-between text-center">
                            <p class="like-info"><span class="align-middle"><i class="fa fa-heart"></i></span> Có 4 lượt thích</p>
                            <div class="col-sm-4 text-center my-2 my-sm-0">
                                <!-- <p class="comment-count"><span class="align-middle"><i class="fa fa-comment"></i></span> 06 Comments</p> -->
                            </div>
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            </ul>
                        </div>
                        <div class="navigation-area">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                                    <div class="thumb">
                                        <a href="#">
                                            <img class="img-fluid" src="img/post/quaylai.png" alt="">
                                        </a>
                                    </div>
                                    <div class="arrow">
                                        <a href="#">
                                            <span class="lnr text-white ti-arrow-left"></span>
                                        </a>
                                    </div>
                                    <div class="detials">
                                        <p>Bài trước</p>
                                        <a href="#">
                                            <h4>Công nghệ tích hợp</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                                    <div class="detials">
                                        <p>Bài tiếp theo</p>
                                        <a href="#">
                                            <h4>tốc độ</h4>
                                        </a>
                                    </div>
                                    <div class="arrow">
                                        <a href="#">
                                            <span class="lnr text-white ti-arrow-right"></span>
                                        </a>
                                    </div>
                                    <div class="thumb">
                                        <a href="#">
                                            <img class="img-fluid" src="img/post/tieptheo.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-author">
                        <div class="media align-items-center">
                            <img src="img/blog/tacgia.png" alt="">
                            <div class="media-body">
                                <a href="#">
                                    <h4>Bùi Huy Hoàng</h4>
                                </a>
                                <p>Laptop cũng có khả năng đa nhiệm ổn, sử dụng nhiều ứng dụng làm việc, nhắn tin... cùng lúc, mở 20 - 30 tab Chrome không bị đứng.</p>
                            </div>
                        </div>
                    </div>
                    <div class="comments-area">
                        <h4>02 Comments</h4>
                        <div class="comment-list">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="img/comment/binhluan1.png" alt="">
                                    </div>
                                    <div class="desc">
                                        <p class="comment">
                                        Dung lượng ổ cứng 256 GB khá thoải mái với dân văn phòng dùng để lưu trữ tài liệu, phim ảnh...
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h5>
                                                    <a href="#">Nguyễn Phi Khanh</a>
                                                </h5>
                                                <p class="date">4/11/2018 at 3:12 pm </p>
                                            </div>
                                            <div class="reply-btn">
                                                <a href="#" class="btn-reply text-uppercase">Trả lời</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="img/comment/binhluan2.png" alt="">
                                    </div>
                                    <div class="desc">
                                        <p class="comment">
                                        Máy tốt trong tầm giá vừa hợp vs nhu cầu học tâoj của t nâng cấp thêm khe ram nữa thì ngon
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h5>
                                                    <a href="#">Từ Vĩ Khang</a>
                                                </h5>
                                                <p class="date">12/3/2019 at 8:19 am </p>
                                            </div>
                                            <div class="reply-btn">
                                                <a href="#" class="btn-reply text-uppercase">Trả lời</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>