<div class="col-lg-4">
    <div class="blog_right_sidebar">
        <aside class="single_sidebar_widget search_widget">
            <form action="#">
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder='Từ khóa' onfocus="this.placeholder = ''" onblur="this.placeholder = 'Từ khóa'">
                        <div class="input-group-append">
                            <button class="btn" type="button"><i class="ti-search"></i></button>
                        </div>
                    </div>
                </div>
                <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Tìm kiếm</button>
            </form>
        </aside>

        <aside class="single_sidebar_widget post_category_widget">
            <h4 class="widget_title">Mục lục</h4>
            <ul class="list cat-list">
                <li>
                    <a href="#" class="d-flex">
                        <p>Apple - Dòng laptop cao cấp</p>
                        <p>&nbsp(37)</p>
                    </a>
                </li>
                <li>
                    <a href="#" class="d-flex">
                        <p>Dell - Dòng laptop sinh viên</p>
                        <p>&nbsp(10)</p>
                    </a>
                </li>
                <li>
                    <a href="#" class="d-flex">
                        <p>Asus - Dòng laptop tầm trung</p>
                        <p>&nbsp(03)</p>
                    </a>
                </li>
                <li>
                    <a href="#" class="d-flex">
                        <p>HP - Dòng laptop tương đối</p>
                        <p>&nbsp(11)</p>
                    </a>
                </li>
                <li>
                    <a href="#" class="d-flex">
                        <p>Lenovo - Dong laptop văn phòng</p>
                        <p>&nbsp(21)</p>
                    </a>
                </li>
                <li>
                    <a href="#" class="d-flex">
                        <p>MSI - Dòng laptop game thủ</p>
                        <p>&nbsp(09)</p>
                    </a>
                </li>
            </ul>
        </aside>

        <aside class="single_sidebar_widget popular_post_widget">
            <h3 class="widget_title">Bài đăng gần đây</h3>
            <div class="media post_item">
                <img src="img/post/camnhan1.png" alt="post">
                <div class="media-body">
                    <a href="single-blog.html">
                        <h3>Tôi cảm thấy chất lượng ...</h3>
                    </a>
                    <p>12 giờ trước</p>
                </div>
            </div>
            <div class="media post_item">
                <img src="img/post/camnhan2.png" alt="post">
                <div class="media-body">
                    <a href="single-blog.html">
                        <h3>Nhìn chung giá cả...</h3>
                    </a>
                    <p>8 giờ trước</p>
                </div>
            </div>
            <div class="media post_item">
                <img src="img/post/camnhan3.png" alt="post">
                <div class="media-body">
                    <a href="single-blog.html">
                        <h3>Giá thành thật rẻ</h3>
                    </a>
                    <p>5 giờ trước</p>
                </div>
            </div>
            <div class="media post_item">
                <img src="img/post/camnhan4.png" alt="post">
                <div class="media-body">
                    <a href="single-blog.html">
                        <h3>Tôi rất thích laptop ...</h3>
                    </a>
                    <p>1 giờ trước</p>
                </div>
            </div>
        </aside>
        <aside class="single_sidebar_widget tag_cloud_widget">
            <h4 class="widget_title">Chọn theo cách riêng của bạn</h4>
            <ul class="list">
                <li>
                    <a href="#">Công nghệ mới</a>
                </li>
                <li>
                    <a href="#">Phong cách</a>
                </li>
                <li>
                    <a href="#">Tốc độ xử lý</a>
                </li>
                <li>
                    <a href="#">Ram</a>
                </li>
                <li>
                    <a href="#">Cá tính</a>
                </li>
                <li>
                    <a href="#">Bền đẹp</a>
                </li>
                <li>
                    <a href="#">Thiết kế độc đáo</a>
                </li>
                <li>
                    <a href="#">Dịch vụ</a>
                </li>
            </ul>
        </aside>


        <aside class="single_sidebar_widget instagram_feeds">
            <h4 class="widget_title">Khách hàng thân thiết</h4>
            <ul class="instagram_row flex-wrap">
                <li>
                    <a href="#">
                        <img class="img-fluid" src="img/post/khthanthiet1.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img class="img-fluid" src="img/post/khthanthiet2.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img class="img-fluid" src="img/post/khthanthiet3.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img class="img-fluid" src="img/post/khthanthiet4.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img class="img-fluid" src="img/post/khthanthiet5.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img class="img-fluid" src="img/post/khthanthiet6.png" alt="">
                    </a>
                </li>
            </ul>
        </aside>


        <aside class="single_sidebar_widget newsletter_widget">
            <h4 class="widget_title">Cập nhận tin mới</h4>

            <form action="#">
                <div class="form-group">
                    <input type="email" class="form-control" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" placeholder='Email' required>
                </div>
                <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Theo dõi</button>
            </form>
        </aside>
    </div>
</div>
</div>
</div>
</section>
<!--================Blog Area =================-->