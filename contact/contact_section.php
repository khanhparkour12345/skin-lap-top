<!-- ================ contact section start ================= -->
<section class="contact-section">
    <div class="container">
        <div class="d-none d-sm-block mb-5 pb-4">
            <div id="map" style="height:0px; position: relative; overflow: hidden;"> </div>
            <!-- Hoang (Tao thấy đoạn này không cần thiết) -->
            <!-- <script>
                    function initMap() {
                        var uluru = {
                            lat: -25.363,
                            lng: 131.044
                        };
                        var grayStyles = [{
                            featureType: "all",
                            stylers: [{
                                saturation: -90
                            }, {
                                lightness: 50
                            }]
                        }, {
                            elementType: 'labels.text.fill',
                            stylers: [{
                                color: '#ccdee9'
                            }]
                        }];
                        var map = new google.maps.Map(document.getElementById('map'), {
                            center: {
                                lat: -31.197,
                                lng: 150.744
                            },
                            zoom: 9,
                            styles: grayStyles,
                            scrollwheel: false
                        });
                    }
                </script>

                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&amp;callback=initMap">
                </script> -->
            <!-- /Hoang (T thấy đoạn này không cần thiết) -->

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.513849363189!2d106.69912411410338!3d10.771900592324563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f40c7a0f411%3A0xe272a9c70ba4a66e!2zNjUgxJDGsOG7nW5nIEh14buzbmggVGjDumMgS2jDoW5nLCBC4bq_biBOZ2jDqSwgUXXhuq1uIDEsIEjhu5MgQ2jDrSBNaW5oLCBWaWV0bmFt!5e0!3m2!1sfr!2s!4v1586787790011!5m2!1sfr!2s" width="90%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>


        </div>

        <?php
        include($level . Contact_part . "from.php");
        ?>

        <div class="col-lg-3 offset-lg-1">
            <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-home"></i></span>
                <div class="media-body">
                    <h3>Tòa nhà Laptop GrearVn.</h3>
                    <p>65 đường Huỳnh Thúc Kháng, Bến Nghé, Q1, Tp.HCM</p>
                </div>
            </div>
            <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                <div class="media-body">
                    <h3>+(08) 911784363</h3>
                    <p>Thứ 2 đến thứ 7 (9h - 18h)</p>
                </div>
            </div>
            <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-email"></i></span>
                <div class="media-body">
                    <h3>buihuyhoang9a3@gmail.com</h3>
                    <p>Hãy liên hệ tôi khi bạn cần!</p>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- ================ contact section end ================= -->