 <div class="recent_trip_area">
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-lg-6">
                 <div class="section_title text-center mb_70">
                     <h3>Sản phẩm ưa chuộng</h3>
                 </div>
             </div>
         </div>
         <div class="row">
             <div class="col-lg-4 col-md-6">
                 <div class="single_trip">
                     <div class="thumb">
                         <img src="img/trip/uachuong1.png" alt="">
                     </div>
                     <div class="info">                        
                         <a href="#">
                             <h3>Laptop Apple Macbook Pro</h3>
                         </a>
                         <div class="date">
                             <span>i5 1.4GHz/8GB/128GB</span>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="col-lg-4 col-md-6">
                 <div class="single_trip">
                     <div class="thumb">
                         <img src="img/trip/uachuong2.png" alt="">
                     </div>
                     <div class="info">
                         
                         <a href="#">
                             <h3>Laptop Dell Inspiron 7373</h3>
                         </a>
                         <div class="date">
                             <span>i5 8250U/8GB/256GB/Office365/Win10</span>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="col-lg-4 col-md-6">
                 <div class="single_trip">
                     <div class="thumb">
                         <img src="img/trip/uachuong3.png" alt="">
                     </div>
                     <div class="info">
                         
                         <a href="#">
                             <h3>Laptop Dell XPS 13 9370</h3>
                         </a>
                         <div class="date">
                             <span>i7 8550U/8GB/256GB/Office365/Win10</span>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>