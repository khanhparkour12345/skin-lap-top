<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <?php
    $level = "";
    include("config.php");

    include($level . Header_part . "link_header.php");
    ?>
</head>

<body>
    <?php
    //header-start
    include($level . Header_part . "top_header.php");

    //<!-- bradcam_area  -->
    include($level . Contact_part . "bradcam_area_contact.php");

    //<!-- contact section start  -->
    include($level . Contact_part . "contact_section.php");

    // <!-- footer start -->
    include($level . Footer_part . "footer.php");

    //<!-- JS here -->
    include($level . Footer_part . "JS.php");
    ?>
</body>

</html>