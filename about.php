<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <?php
    $level = "";
    include("config.php");

    include($level . Header_part . "link_header.php");
    ?>
</head>

<body>
    <?php
    //header-start
    include($level . Header_part . "top_header.php");

    //<!-- bradcam_area  -->
    include($level . About_part . "about_area.php");

    //<!-- testimonial_area_Our Story  -->
    include($level . About_part . "about_ourstory.php");

    //<!-- enjoy Video  -->
    include($level . About_part . "about_enjoy.php");

    // <!-- recent_trip  -->
    include($level . Destination_part . "destination_recent_trip.php");


    // <!-- footer start -->
    include($level . Footer_part . "footer.php");

    //<!-- JS here -->
    include($level . Footer_part . "JS.php");
    ?>
</body>

</html>