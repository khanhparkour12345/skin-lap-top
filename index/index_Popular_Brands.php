<!-- popular_destination_area_start  -->
<div class="popular_destination_area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section_title text-center mb_70">
                        <h3>Thương hiệu nổi tiếng</h3>
                        <p>
                        Máy tính xách tay ngày càng trở nên thông dụng và phổ biến hơn trong xã hội hiện nay. Do đó, có rất nhiều thương hiệu bắt đầu sản xuất máy tính xác tay và phụ kiện của nó. 
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single_destination">
                        <div class="thumb">
                            <img src="img/destination/dell.png" alt="" />
                        </div>
                        <div class="content">
                            <p class="d-flex align-items-center">
                            Dell
                                <a href="travel_destination.php"> 70% có tại thị trường</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_destination">
                        <div class="thumb">
                            <img src="img/destination/asus.png" alt="" />
                        </div>
                        <div class="content">
                            <p class="d-flex align-items-center">
                            Asus
                                <a href="travel_destination.php"> 82% có tại thị trường</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_destination">
                        <div class="thumb">
                            <img src="img/destination/apple.png" alt="" />
                        </div>
                        <div class="content">
                            <p class="d-flex align-items-center">
                            Apple
                                <a href="travel_destination.php"> 60% có tại thị trường</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_destination">
                        <div class="thumb">
                            <img src="img/destination/acer.png" alt="" />
                        </div>
                        <div class="content">
                            <p class="d-flex align-items-center">
                            Acer
                                <a href="travel_destination.php"> 75% có tại thị trường</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_destination">
                        <div class="thumb">
                            <img src="img/destination/HP.png" alt="" />
                        </div>
                        <div class="content">
                            <p class="d-flex align-items-center">
                            HP
                                <a href="travel_destination.php"> 75% có tại thị trường</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_destination">
                        <div class="thumb">
                            <img src="img/destination/lenovo.png" alt="" />
                        </div>
                        <div class="content">
                            <p class="d-flex align-items-center">
                            Lenovo
                                <a href="travel_destination.php"> 70% có tại thị trường</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- popular_destination_area_end  -->